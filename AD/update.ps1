Function ConvertFileToUnicode([string]$filePath, [string]$sourceEnc){
		$encFrom = [System.Text.Encoding]::GetEncoding($sourceEnc)
		$bytes = Get-Content $filePath -Encoding Byte
    $encFrom.GetString($bytes) > $filePath
}

Function AskArgumentsIfNeeded($arguments) {
  $result = @{
    server = ""
    rootDN = ""
    user = ""
    password = ""
    inFile = ""
    outFile = ""
  }
  Write-Host "Usage with arguments:"
  Write-Host "-server <ldap server> -rootDN <root disting. name> -user <ldap user> -password <password> -in <input file name> -out <output file name>"
  for ( $i = 0; $i -lt $arguments.count; $i++ ) {
    if ($arguments[ $i ] -eq "-server") { $result.server = $arguments[ ++$i ] }
    if ($arguments[ $i ] -eq "-rootDN") { $result.rootDN = $arguments[ ++$i ] }
    if ($arguments[ $i ] -eq "-user") { $result.user = $arguments[ ++$i ] }
    if ($arguments[ $i ] -eq "-password") { $result.password = ConvertTo-SecureString $arguments[ ++$i ] -AsPlainText -Force }
    if ($arguments[ $i ] -eq "-in") { $result.inFile = $arguments[ ++$i ] }
    if ($arguments[ $i ] -eq "-out") { $result.outFile = $arguments[ ++$i ] }
  }
  if (!$result.server) { $result.server = Read-Host "Server" }
  if (!$result.rootDN) { $result.rootDN = Read-Host "RootDN" }
  if (!$result.user) { $result.rootDN = Read-Host "User" }
  if (!$result.password) { $result.password = Read-Host "Password" -AsSecureString }
  if (!$result.inFile) { $result.inFile = Read-Host "Input file name" }
  if (!$result.outFile) { $result.outFile = Read-Host "Output file name" }
  return $result
}

#parsePhones("123,321,123,5,+781273712831,+98239818239")
Function parsePhones($phoneStr) {
  $result = @{
    mobile = ""
    office = ""
    otherMobile = @()
    otherOffice = @()
    excluded = @()
  }
  $phones = @($phoneStr.split(",")).Where({ $_ -and $_ -ne "-" })
  $mobile = @($phones).Where({ $_.length -gt 9 })
  $office = @($phones).Where({ $_.length -eq 3 })
  $result.excluded = @(@($phones) | Select-Object -unique | Where-Object { $_ -and $mobile -notcontains $_ -and $office -notcontains $_ })
  $result.mobile = $mobile | Select-Object -unique -first 1
  $result.office = $office | Select-Object -unique -first 1
  $result.otherMobile = $mobile | Select-Object -unique -skip 1
  $result.otherOffice = $office | Select-Object -unique -skip 1
  return $result
}

Function updateUser($user, $data) {
  $result = ""
  try {
		$clearFields = @{}
    $updateFields = @{}
		$clearFields.displayNamePrintable = ""
		$clearFields.otherTelephone = ""
		$clearFields.otherMobile = ""
    if ($data.FIO) { $updateFields.middleName = $data.FIO }
		else { $clearFields.middleName = "" }
    if ($data.OFFICE) { $updateFields.physicalDeliveryOfficeName = $data.OFFICE }
		else { $clearFields.physicalDeliveryOfficeName = "" }
    if ($data.PHONES){
      $phones = parsePhones($data.PHONES)
      if ($phones.office) { $updateFields.telephoneNumber = $phones.office }
			else { $clearFields.telephoneNumber = "" }
      if ($phones.otherOffice) { $updateFields.otherTelephone = $phones.otherOffice }
      if ($phones.mobile) { $updateFields.mobile = $phones.mobile }
			else { $clearFields.mobile = "" }
      if ($phones.otherMobile) { $updateFields.otherMobile = $phones.otherMobile }
      if ($phones.excluded) { $result = "Excluded phones: $($phones.excluded)" }
    }
    if ($data.CITY) { $updateFields.l = $data.CITY }
		else { $clearFields.l = "" }
    if ($data.DEPARTMENT) { $updateFields.department = $data.DEPARTMENT }
		else { $clearFields.department = "" }
    if ($data.POSITION) { $updateFields.title = $data.POSITION }
		else { $clearFields.title = "" }

		$clearFields.GetEnumerator() | ForEach-Object { Set-ADUser $user -Clear $_.Name }
    Set-ADUser $user -Replace $updateFields
  } catch {
    $result = $_
  }
  return $result
}

Function error($msg){
  Write-Host $msg -foregroundcolor red -backgroundcolor black
}

Function main($arguments) {
  $ErrorActionPreference = "Stop"
  if (Get-Module -ListAvailable -Name ActiveDirectory) {
    Import-Module ActiveDirectory
    #get arguments
    $arguments = AskArgumentsIfNeeded($arguments)
    $out = @()
    try {
      #connect to ldap
      $drive = New-PSDrive -Name parma -PSProvider ActiveDirectory -Root $arguments.rootDN -Server $arguments.server -Credential (New-Object System.Management.Automation.PSCredential ($arguments.user, $arguments.password))
      try {
        cd parma:
        #update users
        ConvertFileToUnicode $arguments.inFile "Windows-1251"
        $csvUsers = Import-CSV $arguments.inFile -Delimiter ";"
        ForEach ($csvUser in $csvUsers) {
          $message = ""
          if ($csvUser.EMAIL){
            $adUser = Get-ADUser -LDAPFilter "(mail=$($csvUser.EMAIL))" -Properties *
            if ($adUser){
              $message = updateUser $adUser -data $csvUser
            } else {
              $message = "User with mail $($csvUser.EMAIL) not found"
            }
          } else {
            $message = "User doesn't have email address: $csvUser"
          }
          if ($message){
            $props = @{
              input = "$csvUser"
              error = $message
            }
            $out += New-Object PSObject -Property $props
            error $message
          }
        }
      } finally {
        cd $(Get-PSDrive -PSProvider FileSystem | Select -first 1).Root
        Remove-PSDrive $drive
        $out | export-csv -Path $arguments.outFile -NoTypeInformation
      }
    } catch {
      error $_
      error $_.Exception
    }
  } else {
    error "Please install ActiveDirectory module. https://technet.microsoft.com/en-us/library/cc730825(v=ws.11).aspx - Remote Server Administration Tool"
  }
}

#Set-ExecutionPolicy RemoteSigned
main($args)
